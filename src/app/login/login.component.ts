import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

import { NzFormTooltipIcon } from 'ng-zorro-antd/form';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  validateForm!: UntypedFormGroup;

  submitForm(): void {
    if (this.validateForm.valid) {
      console.log('submit', this.validateForm.value);
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }


  constructor(private fb: UntypedFormBuilder) { }


  ngOnInit(): void {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }


  onDigitInput(event: any, previousElement: any, nextElement: any): void {
    if (event.code !== 'Backspace' && nextElement !== null) {
      nextElement.focus();
    }

    if (event.code === 'Backspace' && previousElement !== null) {
      previousElement.focus();
      previousElement.value = '';
    }
  }


/*   skip(){
    function skipToNextInputOnMaxLength(input: HTMLInputElement) {
      input.addEventListener("keyup", (event) => {
        if (input.value.length === Number(input.getAttribute("maxlength"))) {
          let inputs = document.getElementsByTagName("input");
          let currentIndex = Array.from(inputs).indexOf(event.target);
          if (currentIndex + 1 < inputs.length) {
            inputs[currentIndex + 1].focus();
          }
        }
      });
    }
    const input = document.querySelector("input");
    skipToNextInputOnMaxLength(input);
  } */
}